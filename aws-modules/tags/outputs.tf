output "Environment" {
  value       = var.Environment
}

output "Name" {
  value       = var.Name
}


output "Stack" {
  value       = var.Stack
}

output "CommitId" {
  value       = var.CommitId
}

output "Product" {
  value       = var.Product
}

output "tags" {
  description = "AWS tags to apply to all resources"
  value       = {
    Product         = var.Product
    CommitId        = var.CommitId
    Environment     = var.Environment
    Name            = var.Name
    Stack           = var.Stack
  }
}
