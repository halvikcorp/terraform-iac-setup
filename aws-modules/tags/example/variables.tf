variable "region" {
  description = "AWS Region"
  default     = "us-east-1"
}

variable "shared_cred_file" {
  description = "shared file path"
  default     = "~/.aws/credentials"
}

variable "profile" {
  description = "default profile"
  default     = "psb"
}

variable "ami" {
  description = "AMI"
  default     = "ami-05080f375ff5f2bbc"
}

variable "instance_type" {
  description = "EC2 instance type"
  default     = "t2.small"
}

variable "key_name" {
  description = "keyname"
  default     = "psb-lab-dev-key-pair-1"
}

variable "subnet" {
  description = "subnet where ec2 needs to be launched"
  default     = "subnet-0383101af50ccc089"
}

variable "securityGroups" {
  type    = list
  default = ["sg-09f329e505dddb6e8", "sg-0c10aea6a3f6ba167"]
}

variable "instance_count" {
  type        = number
  description = "Count of ec2 instances to create"
  default     = 1
}
