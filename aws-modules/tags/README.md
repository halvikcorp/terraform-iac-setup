PSB Common Tags Module
===========

A terraform module to provide shared variables and properties among other modules

Module Tags
----------------------

- `Product`                - Product Name
- `CommitId`               - GIT commit id needs to specified
- `Environment`            - Environment (DEV/UAT/PROD)
- `Name`                   - Name of the resource
- `Stack`                  - Stack to which the resource falls under


Usage
-----

```hcl
# Code  added to in main.tf to invoke common-tags module for the resource you are trying to provision:
module "common-tags" {

    source = "git::https://gitlab.com/jagssp/terraform.git//aws-modules/tags"

    Name                   = "PaaS-DEV-EC2-1"
    Stack                  = "Middleware"
    CommitId               = "775d7a0d3f1ad7edc489a7ea78854a8c5f39344e"
    BusinessProduct        = "InfraTest"
    ProductLine            = "EBPL"
    ComponentID            = "PA-S"
    Environment            = "DEV"
    Product                = "PaaS"
}

resource "aws_instance" "server1" { 
  tags = module.common-tags.tags
}
```
Output
-----
```hcl
 tags                       
          CommitId        = "775d7a0d3f1ad7edc489a7ea78854a8c5f39344e"
          Environment     = "DEV"
          LastUpdate      = "Today"
          Name            = "PaaS-DEV-EC2-1"
          Product         = "PaaS"
          Stack           = "Middleware"
```
