## S3 Bucket & DynamoDB Table

This terraform module creates an S3 bucket with versioning and encryption enabled and a DynamoDB table, to be used for locking terraform runs and thus serialising the concurrent terraform changes.

## S3 Bucket

The module does the following:
* Creates an S3 bucket.
* Enables versioning on it.
* Enables AES encryption on it.

## DynamoDB

The module does the following:
* Creates a dynamo DB table.
* Creates the table with a column called LockID of type string.

#### Prerequisites
This module assumes that the AWS account this is deployed with has permissions to S3 & DynamoDB.

#### Quick start
To get a working S3 bucket & DynamoDB table complete the following step:

1. Inspect variables in the terraform.tfvars file and update as necesssary.
```
bucket_name             = "bucket name in small case"
acl                     = "private|public-read|public-read-write|authenticated-read|aws-exec-read|log-delivery-write"
mfa_delete              = "true|false"
table_name              = "dynamo db table name"
read_capacity           = "The number of read units for this table. If the billing_mode is PROVISIONED, this field is required"
write_capacity          = "The number of write units for this table. If the billing_mode is PROVISIONED, this field is required"
```

2. Run the following to initialize the backend and download modules and plugins.
```
terraform init
```
3. Run the following to valdiate the resource configurations.
```
terraform validate
```
4. Run the following to plan the instance and all resources.
```
terraform plan
```
5. Run the following to create the instance and all resources.
```
terraform apply
```
## Requirements

| Name | Version |
|------|---------|
| template | >= 2.1 |

## Providers

| Name | Version |
|------|---------|
| aws | >= 3.29 |
| terraform | >= 0.14 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| bucket_name | the name of the bucket to create in small case | `string` | n/a | yes |
| allow_destroy_when_objects_present | Setting that allowes the bucket to be destroyed even when not empty | `string` | `no` | yes |
| common_tags | Common tags requried to be added to the resources | `string` | n/a | yes |
| extra-tags | Extra tags requried to be added to the resources | `string` | n/a | yes |
| acl | The access control rules for this bucket | `string` | private | yes |
| table_name | dynamo db table name | `string` | n/a | yes |
| read_capacity | The number of read units for this table. | `string` | n/a | yes |
| write_capacity | The number of write units for this table. | `string` | n/a | yes |
## Outputs

| Name | Description |
|------|-------------|
| bucket_arn | The name of the created bucket |
| bucket_name | The ARN of the created bucket |
| dynamodb_table_name | The name of DynamoDB table created |
| common-tags | The common tags applied for the resoruces |
