provider "aws" {
  region                  = "${var.region}"
  shared_credentials_file = "${var.shared_cred_file}"
  profile                 = "${var.profile}"
} //provider

locals {
  common_tags = {
        ComponentName   = "${var.ComponentName}"
        Product         = "${var.Product}"
        CommitId        = "${var.CommitId}"
        LastUpdate      = "${var.LastUpdate}"
        LastUpdateBy    = "${var.LastUpdateBy}"
        Environment     = "${var.Environment}"
        KeepOn          = "${var.KeepOn}"
        BusinessArea    = "${var.BusinessArea}"
        BusinessProduct = "${var.BusinessProduct}"
        ProductLine     = "${var.ProductLine}"
        PPACode         = "${var.PPACode}"
        Name            = "${var.Name}"
        Stack           = "${var.Stack}"
  }
}


resource "aws_s3_bucket" "encrypted_bucket" {
  bucket = var.bucket_name

  acl = var.acl

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }

  force_destroy = var.allow_destroy_when_objects_present == "yes"

  versioning {
    enabled = true
  }

  tags = merge(var.extra-tags, local.common_tags)
}

resource "aws_dynamodb_table" "dynamodb-terraform-state-lock" {
  name = "${var.dynamolockname}"
  hash_key = "${var.hash_key}"
  read_capacity = "${var.read_capacity}"
  write_capacity = "${var.write_capacity}"

  attribute {
    name = "LockID"
    type = "S"
  }
  tags = merge(local.common_tags, "${local.common_tags}")
}
