# Launching EC2 Module by increasing the root volume by 200GB

This terraform module can be used to launch an EC2 instance with a 50GB root voluve as default but has been increased to 200GB

## Prerequisites
This module assumes that the AWS account this is deployed to has permissions to the AWS Console

## EC2 Instance with EBS Attached Volune 
```hcl
#storing state file in Backend
terraform {
  backend "s3" {
    bucket = "psb-terraform-state-bucket"
    key    = "ec2test.tfstate"
    region = "us-east-1"
    dynamodb_table = "terraform-state-lock-dynamo"
  }
}

##########################################################################
# Adding 200GB to root Volume , if you need addition diskspace please add following code .
##########################################################################

# Launching EC2 Instance
module "ec2_instance" {
  source  = "git::https://prod-cicm.uspto.gov/gitlab/psb/terraform.git//aws-modules/ec2"
  ami                    = "ami-0457db48470150f0d"
  // "please note we need use  AMI which UACS has provided , please check Platform automation team on latest AMI"
  instance_type          = "t2.micro"
  key_name               = "psb-lab-dev-key-pair-1"
  vpc_security_group_ids = ["sg-09f329e505dddb6e8", "sg-0c10aea6a3f6ba167"]
  Environment            = "dev"
  Product                = "http"
  subnet_id              = "subnet-0383101af50ccc089"

  #Root Volume
    root_block_device = [
      {
        volume_type           = "gp2"
        volume_size           = 200
        delete_on_termination = true
      }
    ]

}


# Attaching UACS tags
module "common-tags" {
    source = "git::https://prod-cicm.uspto.gov/gitlab/psb/terraform.git//aws-modules/UACS-TAGS"

    BusinessArea           = "Infra"
    Name                   = "PaaS-DEV-EC2-1"
    Stack                  = "Middleware"
    PPAProgramCode         = "SPAAS0"
    CommitId               = "775d7a0d3f1ad7edc489a7ea78854a8c5f39344e"
    LastUpdateBy           = "sthanneeru1"
    BusinessProduct        = "InfraTest"
    LastUpdate             = "Today"
    ProductLine            = "EBPL"
    ComponentID            = "PA-S"
    KeepOn                 = "Mo+Tu+We+Th+Fr:08-18/Sa:00-23/Su:00-23"
    Environment            = "DEV"
    Product                = "PaaS"
}
```
1. Run the following to initialize the backend and download modules and plugins.
```
terraform init
```
2. Run the following to valdiate the resource configurations.
```
terraform validate
```
3. Run the following to plan the instance and all resources.
```
terraform plan
```
4. Run the following to create the instance and all resources.
```
terraform apply
```
## Requirements

| Name | Version |
|------|---------|
| template | >= 2.1 |

## Providers

| Name | Version |
|------|---------|
| aws | >= 3.29 |
| terraform | >= 0.14 |

