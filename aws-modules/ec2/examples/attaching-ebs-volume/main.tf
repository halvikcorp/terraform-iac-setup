provider "aws" {
  region                  = "us-east-1"
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "psb"
} //provider

# Launching EC2 Instance
module "ec2_instance" {
  source  = "git::https://prod-cicm.uspto.gov/gitlab/psb/terraform.git//aws-modules/ec2"
  ami                    = "ami-0457db48470150f0d"
  // "please note we need use  AMI which UACS has provided , please check Platform automation team on latest AMI"
  instance_type          = "t2.micro"
  key_name               = "psb-lab-dev-key-pair-1"
  vpc_security_group_ids = ["sg-09f329e505dddb6e8", "sg-0c10aea6a3f6ba167"]
  Environment            = "dev"
  Product                = "http"
  subnet_id              = "subnet-0383101af50ccc089"
  availability_zone      = "us-east-1b"

}

#create EBS Volume
resource "aws_volume_attachment" "this" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.this.id
  instance_id = module.ec2_instance.id
}

resource "aws_ebs_volume" "this" {
  availability_zone = "us-east-1b"
  size              = 1

  tags              = module.common-tags.tags
}

# Attaching UACS tags
module "common-tags" {
    source = "git::https://prod-cicm.uspto.gov/gitlab/psb/terraform.git//aws-modules/UACS-TAGS"

    BusinessArea           = "Infra"
    Name                   = "PaaS-DEV-EC2-1"
    Stack                  = "Middleware"
    PPAProgramCode         = "SPAAS0"
    CommitId               = "775d7a0d3f1ad7edc489a7ea78854a8c5f39344e"
    LastUpdateBy           = "fbrensley"
    BusinessProduct        = "InfraTest"
    LastUpdate             = "Today"
    ProductLine            = "EBPL"
    ComponentID            = "PA-S"
    KeepOn                 = "Mo+Tu+We+Th+Fr:08-18/Sa:00-23/Su:00-23"
    Environment            = "DEV"
    Product                = "PaaS"
}
